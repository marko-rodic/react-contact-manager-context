import React, { Component } from "react";
import Contact from "./Contact";
import { Consumer } from "../../context";

class Contacts extends Component {
  render() {
    // the value is coming coming from context provider which is the whole state
    return (
      <Consumer>
        {value => {
          const { contacts } = value;
          return (
            // you can replace <div> with <React.Fragment>
            <React.Fragment>
              <h1 className="display-4 mb2">
                <span className="text-danger">Contact </span> List
              </h1>
              {contacts.map(contact => (
                <Contact key={contact.id} contact={contact} />
              ))}
            </React.Fragment>
          );
        }}
      </Consumer>
    );
  }
}

export default Contacts;
